from django import forms
from unsubscribe_app.models import Feedback

class FeedbackForm(forms.ModelForm):
    class Meta:
        model = Feedback
        widgets= {
            'feedback' : forms.RadioSelect(),
        }