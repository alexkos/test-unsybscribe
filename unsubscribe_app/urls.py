# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .compat import patterns, url

urlpatterns = patterns('unsubscribe_app.views',  # noqa
    url(r'^(?P<user_id>\d*)-(?P<token>.*)/$',
        'unsubscribe', name="unsubscribe_unsubscribe"),
    url(r'^leave-feedback/$',
        'leave_feedback', name="leave.feedback"),
)
