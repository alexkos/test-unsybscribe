# -*- coding: utf-8 -*-

import json

from django.shortcuts import get_object_or_404, render_to_response
from django.http import Http404, HttpResponse
from django.template import RequestContext
from django.shortcuts import redirect

from .compat import get_user_model
from .utils import get_token_for_user
from .signals import user_unsubscribed
from .forms import FeedbackForm


def unsubscribe(request, user_id, token, template='unsubscribe/unsubscribe.html',
                extra_context=None):
    """
    Checks the user token and fires `unsubscribe.signals.user_unsubscribed` and
    returns unsubscribe/unsubscribe.html with extra_context, which could include
    callables and `unsubscribe_user`, which is the user that is unsibscribing.
    """
    User = get_user_model()
    user = get_object_or_404(User, pk=user_id)
    if not token == get_token_for_user(user):
        raise Http404

    context = RequestContext(request)

    if extra_context is None:
        extra_context = {}
    for key, value in extra_context.items():
        context[key] = callable(value) and value() or value

    user_unsubscribed.send(sender=User, user=user)
    user.subscribe = False
    user.save()

    form = FeedbackForm()

    return render_to_response(template, 
                             {'unsubscribe_user': user,
                              'form': form
                             }, 
                             context_instance=context)

def leave_feedback(request):
    response_data = {}
    
    if request.is_ajax() and request.method == 'POST':
        form = FeedbackForm(request.POST)

        if form.is_valid():
            form.save()

    return HttpResponse(json.dumps(response_data), content_type="application/json")