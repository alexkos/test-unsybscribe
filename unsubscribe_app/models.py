from django.db import models
from users.models import User


FEEDBACK_VARIANT = (
    ('not_want', 'I no longer want to receive these emails'),
    ('not_signed', 'I never signed up for this mailing list'),
    ('inappropriate', 'The emails inappropriate'),
    ('spam', 'he emails are spam and should be reported'),
    ('other', 'feel in reason below'),
)


class Feedback(models.Model):
    feedback = models.CharField(max_length=15, 
                                choices=FEEDBACK_VARIANT,
                                default=None)
    user = models.ForeignKey(User)
