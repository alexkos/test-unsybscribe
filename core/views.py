from django.shortcuts import render
from django.views.generic.base import TemplateView
from unsubscribe_app.utils import get_token_for_user


class HomePageView(TemplateView):
    template_name = "home.html"

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated():
            token = get_token_for_user(self.request.user)
            context['token'] = token

        return context