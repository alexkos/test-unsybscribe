from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.
class User(AbstractUser):
    subscribe = models.BooleanField('Subscribe', default=False)